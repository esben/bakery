FROM debian:8
LABEL maintainer="Esben Haabendal <esben@haabendal.dk>"

ARG DEBIAN_FRONTEND=noninteractive

# /bin/sh should be bash
RUN echo "dash dash/sh boolean false" | debconf-set-selections \
 && dpkg-reconfigure dash

# Install packages
RUN apt-get -q update && apt-get -q -y install \
      python python-setuptools python-ply sudo \
      python-pysqlite2 git lsb-release \
 && rm -rf /var/lib/apt/lists/*

# Install OE-lite bakery
ADD . /opt/bakery/
RUN cd /opt/bakery \
 && ./setup.py install

# Add non-root user and use it instead of root from here on
RUN useradd -m -s /bin/bash user \
 && echo user ALL=NOPASSWD: ALL > /etc/sudoers.d/user
USER user
